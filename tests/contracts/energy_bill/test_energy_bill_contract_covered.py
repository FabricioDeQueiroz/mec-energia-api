from datetime import date
import pytest
from django.test import TestCase
from rest_framework.test import APIClient
from contracts.models import Contract, EnergyBill
from tests.test_utils import create_objects_test_utils, dicts_test_utils

@pytest.mark.django_db
class TestEnergyBillContractCovered:
    def setup_method(self):
        self.university_dict = dicts_test_utils.university_dict_2
        self.university = create_objects_test_utils.create_test_university(self.university_dict)
        
        self.user_dict = dicts_test_utils.university_user_dict_1
        self.user = create_objects_test_utils.create_test_university_user(self.user_dict, self.university)
        self.client = APIClient()
        self.client.login(
            email=self.user_dict['email'],
            password=self.user_dict['password'])

        self.distributor_dict = dicts_test_utils.distributor_dict_1
        self.distributor = create_objects_test_utils.create_test_distributor(self.distributor_dict, self.university)

        self.consumer_unit_dict_1 = dicts_test_utils.consumer_unit_dict_1
        self.consumer_unit_dict_2 = dicts_test_utils.consumer_unit_dict_2
        self.consumer_unit_dict_3 = dicts_test_utils.consumer_unit_dict_3
        self.consumer_unit_1 = create_objects_test_utils.create_test_consumer_unit(self.consumer_unit_dict_1, self.university)
        self.consumer_unit_2 = create_objects_test_utils.create_test_consumer_unit(self.consumer_unit_dict_2, self.university)
        self.consumer_unit_3 = create_objects_test_utils.create_test_consumer_unit(self.consumer_unit_dict_3, self.university)


    # CT1: Não elegível, oldest_contract é inválido
    def test_energy_bill_not_covered_invalid_oldest_contract(self):
        no_contracts_consumer_unit = self.consumer_unit_1
        
        assert EnergyBill.check_energy_bill_covered_by_contract(no_contracts_consumer_unit, None) == False


    # CT2: Não elegível, latest_contract é inválido
    def test_energy_bill_not_covered_invalid_latest_contract(self):
        no_contracts_consumer_unit = self.consumer_unit_1

        assert EnergyBill.check_energy_bill_covered_by_contract(no_contracts_consumer_unit, date.today) == False 

    
    # CT3: Elegível, fatura coberta pelo contrato mais antigo
    def test_energy_bill_covered_by_oldest_contract(self):
        consumer_unit = self.consumer_unit_2

        # start_date = 2023/01/01
        oldest_contract = create_objects_test_utils.create_test_contract(
            dicts_test_utils.contract_dict_3, self.distributor, consumer_unit
        )

        # start_date = 2024/01/01
        latest_contract = create_objects_test_utils.create_test_contract(
            dicts_test_utils.contract_dict_4, self.distributor, consumer_unit
        )

        energy_bill_dict = {
            'date': date(year=2023, month = 3, day = 1),
            'invoice_in_reais': 100.00,
            'is_atypical': False,
            'peak_consumption_in_kwh': 100.00,
            'off_peak_consumption_in_kwh': 100.00,
            'peak_measured_demand_in_kw': 100.00,
            'off_peak_measured_demand_in_kw': 100.00,
        }

        # Fatura - date = 2023/03/01
        energy_bill = create_objects_test_utils.create_test_energy_bill(energy_bill_dict, oldest_contract, consumer_unit)

        assert EnergyBill.check_energy_bill_covered_by_contract(consumer_unit, energy_bill.date) == True


    # CT4: Elegível, fatura coberta pelo contrato mais recente
    def test_energy_bill_covered_by_latest_contract(self):
        consumer_unit = self.consumer_unit_2

        # start_date = 2023/01/01
        oldest_contract = create_objects_test_utils.create_test_contract(
            dicts_test_utils.contract_dict_3, self.distributor, consumer_unit
        )

        # start_date = 2024/01/01
        latest_contract = create_objects_test_utils.create_test_contract(
            dicts_test_utils.contract_dict_4, self.distributor, consumer_unit
        )

        energy_bill_dict = {
            'date': date(year=2024, month = 3, day = 1),
            'invoice_in_reais': 100.00,
            'is_atypical': False,
            'peak_consumption_in_kwh': 100.00,
            'off_peak_consumption_in_kwh': 100.00,
            'peak_measured_demand_in_kw': 100.00,
            'off_peak_measured_demand_in_kw': 100.00,
        }

        # Fatura - date = 2024/03/01
        energy_bill = create_objects_test_utils.create_test_energy_bill(energy_bill_dict, latest_contract, consumer_unit)

        assert EnergyBill.check_energy_bill_covered_by_contract(consumer_unit, energy_bill.date) == True

    
    # CT5: Não elegível, fatura não é coberta por nenhum contrato
    def test_energy_bill_not_covered_by_neither_contract(self):
        consumer_unit = self.consumer_unit_3

        # start_date = 2024/01/01
        contract = create_objects_test_utils.create_test_contract(
            dicts_test_utils.contract_dict_4, self.distributor, consumer_unit
        )

        # Não é possível criar uma fatura com data anterior ao 
        # contrato pois retorna uma exceção impedindo.
        # Portanto: Função testada passando uma data diretamente.

        assert EnergyBill.check_energy_bill_covered_by_contract(consumer_unit, date(year = 2022, month = 1, day = 1)) == False