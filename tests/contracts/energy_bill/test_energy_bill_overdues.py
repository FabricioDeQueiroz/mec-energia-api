from datetime import date
import pytest
from django.test import TestCase
from rest_framework.test import APIClient
from contracts.models import Contract, EnergyBill
from tests.test_utils import create_objects_test_utils, dicts_test_utils

@pytest.mark.django_db
class TestEnergyBillOverdues:
    def setup_method(self):
        self.university_dict = dicts_test_utils.university_dict_2
        self.university = create_objects_test_utils.create_test_university(self.university_dict)
        
        self.user_dict = dicts_test_utils.university_user_dict_1
        self.user = create_objects_test_utils.create_test_university_user(self.user_dict, self.university)
        self.client = APIClient()
        self.client.login(
            email=self.user_dict['email'],
            password=self.user_dict['password'])

        self.distributor_dict = dicts_test_utils.distributor_dict_1
        self.distributor = create_objects_test_utils.create_test_distributor(self.distributor_dict, self.university)

        self.consumer_unit_dict_1 = dicts_test_utils.consumer_unit_dict_1
        self.consumer_unit = create_objects_test_utils.create_test_consumer_unit(self.consumer_unit_dict_1, self.university)

        self.contract_dict_1 = dicts_test_utils.contract_dict_1
        self.contract = create_objects_test_utils.create_test_contract(self.contract_dict_1, self.distributor, self.consumer_unit)

        self.energy_bill_dict_1 = dicts_test_utils.energy_bill_dict_1
        self.energy_bill_dict_2 = dicts_test_utils.energy_bill_dict_1

        self.energy_bill_dict_3 = {
            'date': date(year=2024, month = 8, day = 1),
            'invoice_in_reais': 100.00,
            'is_atypical': False,
            'peak_consumption_in_kwh': 100.00,
            'off_peak_consumption_in_kwh': 100.00,
            'peak_measured_demand_in_kw': 100.00,
            'off_peak_measured_demand_in_kw': 100.00,
        }

        self.energy_bill_dict_4 = {
            'date': date(year=2024, month = 7, day = 1),
            'invoice_in_reais': 100.00,
            'is_atypical': False,
            'peak_consumption_in_kwh': 100.00,
            'off_peak_consumption_in_kwh': 100.00,
            'peak_measured_demand_in_kw': 100.00,
            'off_peak_measured_demand_in_kw': 100.00,
        }
        
        # Fatura - date = 2024/08/08
        self.energy_bill_1 = create_objects_test_utils.create_test_energy_bill(self.energy_bill_dict_1, self.contract, self.consumer_unit)

        # Fatura - date = 2024/08/08
        self.energy_bill_2 = create_objects_test_utils.create_test_energy_bill(self.energy_bill_dict_2, self.contract, self.consumer_unit)

        # Fatura - date = 2024/08/01
        self.energy_bill_3 = create_objects_test_utils.create_test_energy_bill(self.energy_bill_dict_3, self.contract, self.consumer_unit)

        # Fatura - date = 2024/07/01
        self.energy_bill_4 = create_objects_test_utils.create_test_energy_bill(self.energy_bill_dict_4, self.contract, self.consumer_unit)


    # CT1: Verifica se é retornado as faturas vencidas
    def test_verify_overdue_bills(self):
        overdue_bills = [self.energy_bill_3, self.energy_bill_4] # Lista de Faturas Vencidas da consumer_unit

        overdue_bills_method = EnergyBill.check_overdue_energy_bills(self.consumer_unit, date(year=2024, month = 8, day = 8))

        assert set(overdue_bills_method) == set(overdue_bills)

    
    # CT2: Verifica se é retornado as faturas vencidas em ordem de prioridade
    def test_verify_prioritized_overdue_bills(self):
        overdue_bills = [self.energy_bill_4, self.energy_bill_3] # Lista de Faturas Vencidas com a mais antiga primeiro

        assert EnergyBill.check_overdue_energy_bills(self.consumer_unit, date(year=2024, month = 8, day = 8)) == overdue_bills

    
    